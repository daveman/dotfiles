#!/bin/bash
# Script from https://askubuntu.com/questions/193072/how-to-use-the-new-adobe-source-code-pro-font
# latest URL is "Source code (zip)" link at bottom of:
# https://github.com/adobe-fonts/source-code-pro/releases/tag/2.030R-ro/1.050R-it
FONT_NAME="SourceCodePro"
URL="https://github.com/adobe-fonts/source-code-pro/archive/2.030R-ro/1.050R-it.zip"
mkdir /tmp/adodefont
cd /tmp/adodefont
wget ${URL} -O ${FONT_NAME}.zip
unzip -o -j ${FONT_NAME}.zip
mkdir -p ~/.fonts
cp *.otf ~/.fonts
fc-cache -f -v
